Кожевников Михаил Сергеевич, 
Группа: БСБО-07-20,
Вариант: 13 (7, 11)  

# Описание

## Часть 1
main.py принимает два числа с плавающей точкой в качестве аргументов командной строки, определяет количество цифр после запятой в каждом числе и выводит информацию о том, в каком числе их больше. Если количество цифр после запятой одинаково, программа сообщает об этом.

## Часть 2
Программа использует библиотеку psycopg2 для подключения к базе данных PostgreSQL, выполненяет SQL-запрос, записывает результат в файл и закрывает соединение.

# Дополнительное задание
В основной работе  нет общих зависимостей между job-ами, поэтому кешировать зависимости не получится. Но можно создать секцию для кэша.