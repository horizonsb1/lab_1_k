import sys

def count_decimal_digits(number):
    decimal_part = str(number).split('.')[-1]
    return len(decimal_part)

def compare_decimal_digits(number1, number2):
    decimal_digits_number1 = count_decimal_digits(number1)
    decimal_digits_number2 = count_decimal_digits(number2)

    if decimal_digits_number1 > decimal_digits_number2:
        print(f"В числе {number1} больше цифр после запятой.")
    elif decimal_digits_number1 < decimal_digits_number2:
        print(f"В числе {number2} больше цифр после запятой.")
    else:
        print("Количество цифр после запятой в обоих числах одинаково.")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python main.py <number1> <number2>")
        sys.exit(1)

    number1 = float(sys.argv[1])
    number2 = float(sys.argv[2])

    compare_decimal_digits(number1, number2)