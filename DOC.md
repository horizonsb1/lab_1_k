Вариант: 13 (7, 11)  

## Задание: решить задачу из двух частей и организовать структуру проекта на сервисах gitlab.

### Задачи

1. Для первой части организовать проект с системой контроля версий и систему сборки на основе CI/CD. Система CI/CD
   должна собирать приложение запускать его передавая аргумент, который может динамически указать пользователь.
   Результат работы приложения должен быть возвращен из CI/CD в виде артефакта.
2. Для второй части организовать проект с системой контроля версий и систему сборки на основе CI/CD. Проект должен
   состоять из 2-х сервисов: сервиса «приложения» и сервиса с «данными». Сервис «приложения», должен делать запрос в
   сервис с «данными». Система CI/CD должна собирать приложение и подготовить все для запуска или получения результата
   его работы (Например, собрать и запустить сервисы в разных окружениях, где приложение делает запрос к данным и
   результат сохраняет в виде артефакта или подготовить файл для запуска сервисов «docker-compose.yml», которые
   сохраняют результат в файле).

* ### 1 часть
  Создать приложение, которое определяет в какой из двух вещественных переменных больше цифр после запятой

* ### 2 часть
  Составить список учебной группы, включающий Х человек. Для каждого студента указать: фамилию и имя, дату рождения (год, месяц и число), также у каждого студента есть зачетка, в которой указаны: предмет (от трех до пяти), дата экзамена, ФИО преподавателя.
  Программа должна обладать следующим функционалом:
  Вывод виде таблицы ФИО тех студентов, у которых дата сдачи экзамена не раньше n.

### Требования к репозиторию

1. Создать публичный репозиторий на gitlab
2. Оформить структуру проекта на любом языке программирования и сделать первоначальную фиксацию пустого проекта
3. Оформить ветки «master» и «dev»
4. Зафиксировать в ветке «dev» промежуточные этапы разработки
5. В ветку «master» зафиксировать версию 1.0. полностью готовое решение задачи
6. Создать документацию:
    * В корне проекта
        * README.md с кратким описанием и интерфейсом программы в разметке markdown.
        * DOC.md с описанием задачи в разметке markdown

### Функциональные требования

1. Решить задачи на любом языке программирования
2. Решения всех задач должны собираться, запускаться или разворачиваться системой CI/CD
3. Для 1 части, требуется ввод данных, организовать данный ввод динамически средствами CI/CD, а не «жестко закодировать»
4. Для 2 части данные для задачи и приложение должны быть разделены на отдельные изолированные друг от друга сервисы.
   Например, сервис «БД» и сервис «приложения», которое делает запрос к «БД»
5. Данные для части 2 могут храниться в файле или БД

# Дополнительное задание
Настройка кэша для зависимостей загружаемых из интернета и промежуточных вычислений между этапами.