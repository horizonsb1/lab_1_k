import psycopg2
import time

time.sleep(20)

conn = psycopg2.connect(
    dbname="root",
    user="root",
    password="root",
    host="postgres",
    port="5432"
)

query = """
    SELECT 
        Students.first_name, Students.last_name, RecordBooks.subject, RecordBooks.exam_date 
    FROM 
        Students
    JOIN 
        RecordBooks ON Students.student_id = RecordBooks.student_id
    WHERE 
        RecordBooks.exam_date >= '2023-02-01'
"""

cur = conn.cursor()
cur.execute(query)
rows = cur.fetchall()

with open('result.txt', 'w') as file:
    for row in rows:
        file.write(f"{row[0]}\t{row[1]}\n")

cur.close()
conn.close()
